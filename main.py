from argparse import Namespace
from train_nifti import Train
from options.train_options import TrainOptions
from test_nifti import Test

if __name__ == '__main__':

    dataroot = './datasets/my_dataset/A'
    model_name = 'my_model'
    results_dir = './results'

    # defines testing and training subjects
    training_images= ["sub-PAT001.nii.gz", "sub-PAT002.nii.gz"]
    testing_images = ["sub-PAT003.nii.gz"]

    # define options for training
    opt_train = Namespace(checkpoints_dir='./checkpoints', img_names =training_images,
              continue_train=False, crop_size=256, add_ssim=False, add_LPIPS=1, lambda_LPIPS=100,
              dataroot=dataroot,
              dataset_mode='aligned', direction='AtoB', add_L2=False,
              display_env='main', display_freq=400, display_id=1, display_ncols=4, display_port=8097,
              display_server='http://localhost', display_winsize=256,
              enable_validation=False, epoch='latest', epoch_count=1, gan_mode='wgangp', gpu_ids=[0],
              init_gain=0.05, init_type='normal', input_nc=3, isTrain=True, load_iter=0, load_size=286,
              lr_decay_iters=50, max_dataset_size=float("inf"),
              model='pix2pix', n_layers_D=3, name=model_name, ndf=64, netD='basic',
              netG='unet_256', ngf=64, niter=2, niter_decay=3,
              no_flip=False, no_html=True, norm='instance', num_threads=20, output_nc=3, phase='cross_val',
              pool_size=0, preprocess='resize_and_crop', print_freq=1000,
              save_by_iter=False, save_epoch_freq=5, save_latest_freq=5000,
              serial_batches=True, suffix='', switch_gan_loss=0, update_html_freq=5000, verbose=False)

    opt_train.lambda_LPIPS = 500
    opt_train.beta1 = 0.60
    opt_train.lambda_L1 = 1000
    opt_train.lambda_L2 = 10000
    opt_train.lr = 0.0035
    opt_train.batch_size = 4
    opt_train.no_dropout = 'False'
    opt_train.lr_policy = 'linear'

    TrainOptions().print_options(opt_train, parse_bool=False)

    # run training with given options
    MyTrain = Train(opt_train)
    MyTrain.run_train()

    opt_test = Namespace(aspect_ratio=1.0, img_names=testing_images, batch_size=1, checkpoints_dir='./checkpoints',
                         crop_size=256,
                         dataroot=opt_train.dataroot, dataset_mode=opt_train.dataset_mode,
                         direction=opt_train.direction,
                         display_winsize=256, epoch='latest', eval=False, gpu_ids=[0],
                         init_gain=opt_train.init_gain, init_type='normal',
                         input_nc=3, isTrain=False, load_iter=0, load_size=256, max_dataset_size=float('inf'),
                         model=opt_train.model,
                         n_layers_D=3, name=opt_train.name, ndf=64, netD='basic', netG='unet_256', ngf=64,
                         no_dropout=opt_train.no_dropout,
                         no_flip=False, norm=opt_train.norm, ntest=float('inf'), num_test=50, num_threads=20,
                         output_nc=3, phase='cross_val',
                         preprocess='resize_and_crop', results_dir=results_dir, serial_batches=False, suffix='',
                         verbose=False)

    MyTest = Test(opt_test)
    MyTest.run_test()