"""General-purpose test script for image-to-image translation.

Once you have trained your model with train.py, you can use this script to test the model.
It will load a saved model from --checkpoints_dir and save the results to --results_dir.

It first creates model and dataset given the option. It will hard-code some parameters.
It then runs inference for --num_test images and save results to an HTML file.

Example (You need to train models first or download pre-trained models from our website):
    Test a CycleGAN model (both sides):
        python test.py --dataroot ./datasets/maps --name maps_cyclegan --model cycle_gan

    Test a CycleGAN model (one side only):
        python test.py --dataroot datasets/horse2zebra/testA --name horse2zebra_pretrained --model test --no_dropout

    The option '--model test' is used for generating CycleGAN results only for one side.
    This option will automatically set '--dataset_mode single', which only loads the images from one set.
    On the contrary, using '--model cycle_gan' requires loading and generating results in both directions,
    which is sometimes unnecessary. The results will be saved at ./results/.
    Use '--results_dir <directory_path_to_save_result>' to specify the results directory.

    Test a pix2pix model:
        python test.py --dataroot ./datasets/facades --name facades_pix2pix --model pix2pix --direction BtoA

See options/base_options.py and options/test_options.py for more test options.
See training and test tips at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md
See frequently asked questions at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/qa.md
"""
import os
from options.test_options import TestOptions
from data import create_dataset, undo_normalization
from models import create_model
from util.visualizer import save_images
from util import html
from argparse import Namespace
import numpy as np
import nibabel as nib

import torch
import glob

class Test():
    def __init__(self, opt):
        self.opt = opt  # get test options
        # hard-code some parameters for test
        self.opt.num_threads = 0  # test code only supports num_threads = 1
        self.opt.batch_size = 1  # test code only supports batch_size = 1
        self.opt.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
        self.opt.no_flip = True  # no flip; comment this line if results on flipped images are needed.
        self.opt.display_id = -1  # no visdom display; the test code saves the results to a HTML file.
        self.dataset = create_dataset(self.opt)  # create a dataset given opt.dataset_mode and other options
        self.model = create_model(self.opt)  # create a model given opt.model and other options
        self.model.setup(self.opt)  # regular setup: load and print networks; create schedulers
        self.n_channels = self.opt.input_nc
        # n_data = len(dataset)

        # create a website
        web_dir = os.path.join(self.opt.results_dir, self.opt.name,
                               '%s_%s' % (self.opt.phase, self.opt.epoch))  # define the website directory
        self.webpage = html.HTML(web_dir, 'Experiment = %s, Phase = %s, Epoch = %s' % (self.opt.name, self.opt.phase, self.opt.epoch))
        # test with eval mode. This only affects layers like batchnorm and dropout.
        # For [pix2pix]: we use batchnorm and dropout in the original pix2pix. You can experiment it with and without eval() mode.
        # For [CycleGAN]: It should not affect CycleGAN as CycleGAN uses instancenorm without dropout.
        if self.opt.eval:
            self.model.eval()

    def load_current_slice(self, direction, data, slice):
        if direction == 'sag':
            sub_data = {'A': data["A"][:, :, slice:slice + self.n_channels, :, :].squeeze(1),
                        'B': data["B"][:, :, slice:slice + self.n_channels, :, :].squeeze(1),
                        'A_paths': [data['A_paths'][0] + "_" + str(slice)],
                        'B_paths': [data['B_paths'][0] + "_" + str(slice)]}

        if direction == 'cor':
            sub_data = {
                'A': torch.transpose(data["A"][:, :, :, :, slice:slice + self.n_channels].squeeze(1), 3, 1),
                'B': torch.transpose(data["B"][:, :, :, :, slice:slice + self.n_channels].squeeze(1), 3, 1),
                'A_paths': [data['A_paths'][0] + "_" + str(slice)],
                'B_paths': [data['B_paths'][0] + "_" + str(slice)]}

        if direction == 'tra':
            sub_data = {
                'A': torch.transpose(data["A"][:, :, :, slice:slice + self.n_channels, :].squeeze(1), 2, 1),
                'B': torch.transpose(data["B"][:, :, :, slice:slice + self.n_channels, :].squeeze(1), 2, 1),
                'A_paths': [data['A_paths'][0] + "_" + str(slice)],
                'B_paths': [data['B_paths'][0] + "_" + str(slice)]}

        return sub_data

    def fill_result_image(self, direction, result_img, slice):
        # get image results
        visuals = self.model.get_current_visuals()
        if self.n_channels > 1:
            # save only central slice
            if direction == 'sag':
                result_img[direction][slice + int(self.n_channels / 2), :, :] = np.squeeze(np.array(
                    visuals["fake_B"][:, int(self.n_channels / 2), :, :].cpu()))  ## CIPO ADDED .cpu() here !!
            if direction == 'cor':
                result_img[direction][:, slice + int(self.n_channels / 2), :] = np.squeeze(np.array(
                    visuals["fake_B"][:, int(self.n_channels / 2), :, :].cpu()))  ## CIPO ADDED .cpu() here !!
            if direction == 'tra':
                result_img[direction][:, :, slice + int(self.n_channels / 2)] = np.squeeze(np.array(
                    visuals["fake_B"][:, int(self.n_channels / 2), :, :].cpu()))  ## CIPO ADDED .cpu() here !!
        else:
            result_img[direction][slice, :, :] = np.squeeze(np.array(visuals["fake_B"].cpu()))

        return result_img

    def reorient_output(self, result_img, direction):
        # reorient output image to be the same as the original
        if direction == 'sag':
            result_img[direction] = np.swapaxes(result_img[direction], 2, 0)
        if direction == 'cor':
            result_img[direction] = np.swapaxes(result_img[direction], 1, 0)
        if direction == 'tra':
            result_img[direction] = np.swapaxes(result_img[direction], 2, 0)
            result_img[direction] = np.swapaxes(result_img[direction], 1, 0)

        return result_img

    def save_output(self, data, result_img, direction):
        # get header from original nifti file
        f_name_orig = data["B_paths"][0]
        img_orig = nib.load(f_name_orig)
        img_orig_data = img_orig.get_fdata()

        # get original file dimensions and crop output accordingly if needed
        if img_orig_data.shape[0] != 256:
            print('1')
            diff_1 = 256 - img_orig_data.shape[0]
            # img_orig_data = np.pad(img_orig_data, [[int(diff_1 / 2), int(diff_1 / 2)], [0, 0], [0, 0]],
            #                        'constant')
            result_img[direction] = result_img[direction][int(diff_1 / 2):-int(diff_1 / 2), :, :]
        if img_orig_data.shape[1] != 256:
            print('2')
            diff_2 = 256 - img_orig_data.shape[1]
            # img_orig_data = np.pad(img_orig_data, [[0, 0], [int(diff_2 / 2), int(diff_2 / 2)], [0, 0]],
            #                        'constant')
            result_img[direction] = result_img[direction][:, int(diff_2 / 2):-int(diff_2 / 2), :]

        if img_orig_data.shape[2] != 256:
            print('3')
            diff_3 = 256 - img_orig_data.shape[2]
            # img_orig_data = np.pad(img_orig_data, [[0, 0], [0, 0], [int(diff_3 / 2), int(diff_3 / 2)]],
            #                        'constant')
            result_img[direction] = result_img[direction][:, :, int(diff_3 / 2):-int(diff_3 / 2)]

        result_img[direction] = undo_normalization(result_img[direction], 2210)

        # set slices at the beginning and end that were not processed due to sliding window to zero
        if self.n_channels > 1:
            result_img[direction][:, :, :int(self.n_channels / 2)] = 0
            result_img[direction][:, :, -int(self.n_channels / 2):] = 0

        # scale intensities to normal DICOM range
        # result_img = (result_img - np.min(result_img))/(np.max(result_img) - np.min(result_img))*4095

        result_nii = nib.Nifti1Image(result_img[direction], img_orig.affine, img_orig.header)
        f_name_result = os.path.join(self.opt.results_dir, self.opt.name, 'test_' + self.opt.epoch,
                                     os.path.basename(f_name_orig).replace(".nii",
                                                                           "_harmonized_" + direction + ".nii"))

        path_name_result = os.path.split(f_name_result)[0]
        if not os.path.exists(path_name_result):
            os.makedirs(path_name_result)
        result_nii.to_filename(f_name_result)

        return result_img

    def compute_and_save_avg_img(self, result_img, data):
        '''function to compute the average image from synthetic volumes estimated in all 3 directions'''
        f_name_orig = data["B_paths"][0]
        img_orig = nib.load(f_name_orig)

        avg_image = np.mean([result_img['sag'], result_img['tra'], result_img['cor']], axis=0)
        avg_image_nii = nib.Nifti1Image(avg_image, img_orig.affine, img_orig.header)
        avg_image_nii.to_filename(os.path.join(self.opt.results_dir, self.opt.name, 'test_' + self.opt.epoch,
                                               os.path.basename(f_name_orig).replace(".nii",
                                                                                     "_harmonized_avg.nii")))
        return avg_image


    def run_test(self):
        # iteration over test images
        for i, data in enumerate(self.dataset):

            print('Processing dataset {} of {}'.format(i, len(self.dataset)))
            n_slices = data["A"].size()[2]
            #  initialize empty image
            result_img = {'sag': np.squeeze(np.zeros([256, 256, 256])),
                          'tra': np.squeeze(np.zeros([256, 256, 256])),
                          'cor': np.squeeze(np.zeros([256, 256, 256]))}
            # iterate over orientations
            for direction in ['sag', 'cor', 'tra']:
                # iterate over slices
                for slice in range(n_slices - self.n_channels + 1):
                    # load the current slice
                    sub_data = self.load_current_slice(direction, data, slice)
                    # only apply our model to opt.num_test images
                    if i >= self.opt.num_test:
                        break
                    # unpack data from data loader
                    self.model.set_input(sub_data)
                    # run inference
                    self.model.test()
                    # fill result
                    result_img = self.fill_result_image(direction, result_img, slice)
                # reorient output and save result
                result_img = self.reorient_output(result_img, direction)
                result_img = self.save_output(data, result_img, direction)
            # compute and save the average image
            avg_image = self.compute_and_save_avg_img(result_img, data)
        self.webpage.save()
