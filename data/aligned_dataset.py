import os.path
from data.base_dataset import BaseDataset, get_params, get_transform
from data.image_folder import make_dataset
from data import do_normalization
# from PIL import Image
import nibabel as nib
import torch
import numpy as np



class AlignedDataset(BaseDataset):
    """A dataset class for paired image dataset.

    It assumes that the directory '/path/to/data/train' contains image pairs in the form of {A,B}.
    During test time, you need to prepare a directory '/path/to/data/test'.
    """

    def __init__(self, opt):
        """Initialize this dataset class.

        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseDataset.__init__(self, opt)
        if opt.phase in ['train', 'test'] :
            self.dir_AB = os.path.join(opt.dataroot, opt.phase)  # get the image directory
        else : 
            self.dir_AB = opt.dataroot
        self.AB_paths = sorted(make_dataset(self.dir_AB, opt.max_dataset_size, opt.img_names))  # get image paths
        assert(self.opt.load_size >= self.opt.crop_size)   # crop_size should be smaller than the size of loaded image
        self.input_nc = self.opt.output_nc if self.opt.direction == 'BtoA' else self.opt.input_nc
        self.output_nc = self.opt.input_nc if self.opt.direction == 'BtoA' else self.opt.output_nc

    def __getitem__(self, index):
        """Return a data point and its metadata information.

        Parameters:
            index - - a random integer for data indexing

        Returns a dictionary that contains A, B, A_paths and B_paths
            A (tensor) - - an image in the input domain
            B (tensor) - - its corresponding image in the target domain
            A_paths (str) - - image paths
            B_paths (str) - - image paths (same as A_paths)
        """
        # read a image given a random integer index
        AB_path = self.AB_paths[index]

        # identify paths leading to images in domain A and B
        B_path, A_path = AB_path.replace('/A/','/B/'), AB_path

        # load images using nibabel
        imgA, imgB = nib.load(A_path), nib.load(B_path)
        # set the dtype to double
        imgA.set_data_dtype(np.double)
        imgB.set_data_dtype(np.double)
        # get volume data as numpy matrices
        imgA, imgB = imgA.get_data(), imgB.get_data()

        # reorder dimensions
        imgA, imgB = np.swapaxes(imgA,2,0), np.swapaxes(imgB,2,0)

        padding = '3D'
        # zero pad to 256x256x166
        if padding == '3D':
            if imgA.shape[1] != 256 or imgA.shape[2] != 256 or imgA.shape[0] != 256:
                diff_1 = 256 - imgA.shape[1]
                diff_2 = 256 - imgA.shape[2]
                diff_0 = 256 - imgA.shape[0]
                imgA=np.pad(imgA,[[int(diff_0/2),int(diff_0/2)],[int(diff_1/2),int(diff_1/2)],[int(diff_2/2),int(diff_2/2)]],'constant')
                diff_1 = 256 - imgB.shape[1]
                diff_2 = 256 - imgB.shape[2]
                diff_0 = 256 - imgB.shape[0]
                imgB=np.pad(imgB,[[int(diff_0/2),int(diff_0/2)],[int(diff_1/2),int(diff_1/2)],[int(diff_2/2),int(diff_2/2)]],'constant')
        else:
            # zero pad to square 256x256 slices
            if imgA.shape[1] != 256 or imgA.shape[2] != 256:
                diff_1 = 256 - imgA.shape[1]
                diff_2 = 256 - imgA.shape[2]
                imgA=np.pad(imgA,[[0,0],[int(diff_1/2),int(diff_1/2)],[int(diff_2/2),int(diff_2/2)]],'constant')
                diff_1 = 256 - imgB.shape[1]
                diff_2 = 256 - imgB.shape[2]
                imgB=np.pad(imgB,[[0,0],[int(diff_1/2),int(diff_1/2)],[int(diff_2/2),int(diff_2/2)]],'constant')

        # define shapes of images to make them squared
        sizeA, sizeB = imgA.shape, imgB.shape
        imgA, imgB = np.reshape(imgA,[1,sizeA[0],sizeA[1],sizeA[2]]), np.reshape(imgB,[1,sizeB[0],sizeB[1],sizeB[2]])

        # hard-coded threshold for MP-RAGE images to kill hyperintense values that might lead to images that are too dark after normalization
        thr_A = 2210
        thr_B = 1870

        # thresholding
        imgA[imgA>thr_A] = thr_A
        imgA[imgA <0] = 0
        imgB[imgB > thr_B] = thr_B
        imgB[imgB<0] = 0

        # ((img/max_)  - 0.5 )/0.5 / as implemented in pytorch
        imgA = do_normalization(imgA, thr_A)
        imgB = do_normalization(imgB, thr_B)

        # return volumes as tensors of type float
        A,B = torch.from_numpy(imgA).type(torch.FloatTensor), torch.from_numpy(imgB).type(torch.FloatTensor)
        # provide data as a dictionary
        return {'A': A, 'B': B, 'A_paths': A_path, 'B_paths': B_path}

    def __len__(self):
        """Return the total number of images in the dataset."""
        return len(self.AB_paths)
