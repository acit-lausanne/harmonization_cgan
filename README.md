# Introduction
A conditional GAN implementation used for supervised harmonization of Magnetic Resonance Imaging (MRI) images, using L1, L2 and LPIPS [2] reconstruction losses.

This code is based on the implementation of the pix2pix network proposed here: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix [1]. 
Please refer to this link for detailed instructions on the original implementation: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md 

# Usage
A [requirements.txt](https://gitlab.com/acit-lausanne/harmonization_cgan/-/blob/main/requirements.txt) file is provided to install the conda environment.
An example code to train and test a model is provided in the file [main.py](https://gitlab.com/acit-lausanne/harmonization_cgan/-/blob/main/main.py), whereas hyperparameter optimization can be run with Optuna[3] (https://optuna.org/) by running the [optuna_optimization.py](https://gitlab.com/acit-lausanne/harmonization_cgan/-/blob/main/optuna_optimization.py) script.

# Data
The data must be stored in NiFti format in `./datasets/<your dataset name>/A` and `./datasets/<your dataset name>/B` with consistent file names. 

# Citation
Please cite us as:

Ravano, Veronica, et al. "Neuroimaging harmonization using cGANs:  image similarity metrics poorly predict cross-protocol volumetric consistency." Proceedings of the Machine Learning Clinical Neuroscience MICCAI Workshop. 2022. 

# References: 

- [1]: Isola, Phillip, et al. "Image-to-image translation with conditional adversarial networks." Proceedings of the IEEE conference on computer vision and pattern recognition. 2017.

- [2]: Zhang, Richard, et al. "The unreasonable effectiveness of deep features as a perceptual metric." Proceedings of the IEEE conference on computer vision and pattern recognition. 2018.
- [3]: Takuya Akiba, Shotaro Sano, Toshihiko Yanase, Takeru Ohta,and Masanori Koyama. 2019. Optuna: A Next-generation Hyperparameter Optimization Framework. In KDD.
