"""General-purpose training script for image-to-image translation.

This script works for various models (with option '--model': e.g., pix2pix, cyclegan, colorization) and
different datasets (with option '--dataset_mode': e.g., aligned, unaligned, single, colorization).
You need to specify the dataset ('--dataroot'), experiment name ('--name'), and model ('--model').

It first creates model, dataset, and visualizer given the option.
It then does standard network training. During the training, it also visualize/save the images, print/save the loss plot, and save models.
The script supports continue/resume training. Use '--continue_train' to resume your previous training.

Example:
    Train a CycleGAN model:
        python train.py --dataroot ./datasets/maps --name maps_cyclegan --model cycle_gan
    Train a pix2pix model:
        python train.py --dataroot ./datasets/facades --name facades_pix2pix --model pix2pix --direction BtoA

See options/base_options.py and options/train_options.py for more training options.
See training and test tips at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md
See frequently asked questions at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/qa.md
"""

from options.train_options import TrainOptions
import torch
import numpy as np
import nibabel as nib
import optuna
from train_nifti import Train
import os
from test_nifti import Test
import pandas as pd
from argparse import Namespace

results_dir = './results_optuna'
model_name = 'model1'
dataroot = './datasets/my_dataset/A'
opt_train = Namespace(checkpoints_dir='./checkpoints',
              continue_train=False, crop_size=256, add_ssim=False,
              dataroot=dataroot,
              dataset_mode='aligned', direction='AtoB', add_L2=False, add_LPIPS=True,
              display_env='main', display_freq=400, display_id=1, display_ncols=4, display_port=8097,
              display_server='http://localhost', display_winsize=256,
              enable_validation=False, epoch='latest', epoch_count=1, gan_mode='wgangp', gpu_ids=[0],
              init_gain=0.05, init_type='normal', input_nc=3, isTrain=True, load_iter=0, load_size=286,
              lr_decay_iters=50, max_dataset_size=float("inf"),
              model='pix2pix', n_layers_D=3, name=model_name, ndf=64, netD='basic',
              netG='unet_256', ngf=64, niter=2, niter_decay=3,
              no_flip=False, no_html=True, norm='instance', num_threads=30, output_nc=3, phase='cross_val',
              pool_size=0, preprocess='resize_and_crop', print_freq=5000,
              save_by_iter=False, save_epoch_freq=1, save_latest_freq=5000,
              serial_batches=True, suffix='', switch_gan_loss=0, update_html_freq=5000, verbose=False)

opt_train.lambda_LPIPS = 0
opt_train.lambda_L1 = 0
opt_train.lambda_L2 = 10000


def estimate_accuracy_validation(opt_acc) :
    '''function to estimate the accuracy of a given model using validation data'''
    opt_acc.num_threads = 1   # test code only supports num_threads = 1
    opt_acc.batch_size = 1    # test code only supports batch_size = 1
    opt_acc.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
    opt_acc.no_flip = True    # no flip; comment this line if results on flipped images are needed.
    opt_acc.display_id = -1   # no visdom display; the test code saves the results to a HTML file.
    opt_acc.results_dir = './results_optuna_validation'
    L1_acc = []
    MyTest = Test(opt_acc)

    # iteration over validation patients
    for i, data in enumerate(MyTest.dataset):

        print('Processing dataset {} of {}'.format(i, len(MyTest.dataset)))
        n_slices = data["A"].size()[2]
        #  initialize empty image
        result_img = {'sag': np.squeeze(np.zeros([256, 256, 256])),
                      'tra': np.squeeze(np.zeros([256, 256, 256])),
                      'cor': np.squeeze(np.zeros([256, 256, 256]))}
        # recreates volumes based on each orientation
        for direction in ['sag', 'cor', 'tra']:
            # iterate over slices
            for slice in range(n_slices - MyTest.n_channels + 1):
                # load current slice
                sub_data = MyTest.load_current_slice(direction, data, slice)
                # only apply our model to opt.num_test images
                if i >= MyTest.opt.num_test:
                    break
                # unpack data from data loader
                MyTest.model.set_input(sub_data)
                # run inference
                MyTest.model.test()
                # fill resulting volume with estimated slice
                result_img = MyTest.fill_result_image(direction, result_img, slice)
            # reorientation of the volume
            result_img = MyTest.reorient_output(result_img, direction)
            # save the resulting volume as nifti
            result_img = MyTest.save_output(data, result_img, direction)
        # compute and the average image between the three orientations
        avg_image = MyTest.compute_and_save_avg_img(result_img, data)
        # compute the L1 distance between the GT (image in B domain) and the synthetic image
        L1_acc.append(torch.abs(torch.from_numpy(nib.load(data['B_paths'][0]).get_fdata()).type('torch.DoubleTensor')-torch.from_numpy(avg_image).type('torch.DoubleTensor')).mean())
    # compute the average L1 distance over subjects
    mean_MAE = np.mean(L1_acc)

    return mean_MAE

def define_trial(trial, opt) :
    '''
    function to define a trial for Optuna hyperparameter optimization
        Args: opt: training options

    '''
    opt.beta1 = trial.suggest_float("beta1", 0.1, 0.7)
    opt.lr = trial.suggest_loguniform("lr", 1e-4, 1e-1)
    opt.batch_size = trial.suggest_int('batch_size', 1, 5)
    opt.no_dropout = trial.suggest_categorical('no_dropout', ['False', 'True'])
    opt.lr_policy = trial.suggest_categorical('lr_policy', ['cosine', 'linear'])
    MyTrain = Train(opt)

    return MyTrain

def objective(trial) :
    ''' function to run an Oputna trial and estimate its accuracy'''
    # initialize the training options
    opt_train_optuna = opt_train
    # create directory to store checkpoints
    os.makedirs(os.path.join(opt_train.checkpoints_dir, opt_train_optuna.name), exist_ok=True)
    # define a trial for Optuna
    MyTrain = define_trial(trial, opt_train_optuna)
    # run training with values defined by the optuna trial
    MyTrain.run_train()
    # sometimes models diverge, if not:
    if not any([np.isnan(item) for item in MyTrain.model.get_current_losses().values()]) :
        # define options for the validation of accuracy for the given trial
        opt_acc = Namespace(aspect_ratio=1.0, img_names = validation_in, batch_size=1, checkpoints_dir='./checkpoints', crop_size=256,
                  dataroot=opt_train_optuna.dataroot, dataset_mode=opt_train_optuna.dataset_mode, direction=opt_train_optuna.direction,
                  display_winsize=256, epoch='latest', eval=False, gpu_ids=[0], init_gain=opt_train_optuna.init_gain, init_type='normal',
                  input_nc=3, isTrain=False, load_iter=0, load_size=256, max_dataset_size=float('inf'), model=opt_train_optuna.model,
                  n_layers_D=3, name=model_name, ndf=64, netD='basic', netG='unet_256', ngf=64, no_dropout=opt_train_optuna.no_dropout,
                  no_flip=False, norm=opt_train_optuna.norm, ntest=float('inf'), num_test=50, num_threads=20, output_nc=3, phase='val',
                  preprocess='resize_and_crop', results_dir=results_dir, serial_batches=False, suffix='', verbose=False)
        # estimate the MAE for the given trial
        accuracy = estimate_accuracy_validation(opt_acc)
    else:
        # otherwise return NAN
        accuracy = np.nan

    return accuracy

def optuna_optimization() :
    '''
    function to optimize the hyperparameters of the model using Optuna

    Returns:
        trial : the best set of hyperparameters

    '''
    # create one study
    study = optuna.create_study(direction="minimize")
    # we optimize by minimizing the MAE over 20 trials
    study.optimize(objective, n_trials=2)

    pruned_trials = [t for t in study.trials if t.state == optuna.structs.TrialState.PRUNED]
    complete_trials = [t for t in study.trials if t.state == optuna.structs.TrialState.COMPLETE]

    # create dataframe with summary of hyperparameters and performance for each trial
    df = study.trials_dataframe()
    # create folder to store the results
    os.makedirs(results_dir + '/' + model_name, exist_ok=True)
    # save the data frame as CSV
    df.to_csv(results_dir + '/' + model_name + '/optuna_fold_' + str(fold) + '.csv')
    print("Study statistics: ")
    print("  Number of finished trials: ", len(study.trials))
    print("  Number of pruned trials: ", len(pruned_trials))
    print("  Number of complete trials: ", len(complete_trials))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: ", trial.value)

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))

    return trial

def read_optuna_results(fold) :
    '''
    function to retrieve the best model in each fold
    Args:
        fold : (int) of which fold is considered
    Returns :
        best_model : best set of hyperparameters
    '''
    # read dataframe with optuna results
    csv_file = pd.read_csv(os.path.join(results_dir, model_name, 'optuna_fold_'+ str(fold)+'.csv'))
    # find set of hyperparameters with lowest MAE
    best_model = csv_file.loc[csv_file['value'] == np.min(csv_file['value']) ]

    return best_model

def train_test_best_model(trial, best_model_name, train_out):
    '''
    Function to re-train the best model on the whole training set (training + validation)
    and test it in outter testing set

    Args:
        trial : best trial optained with Optuna
        best_model_name : name of the best model
        train_out : set of training images to use
    '''

    # the options for training are initialized
    opt_best_model = opt_train
    opt_best_model.img_names = train_out
    opt_best_model.name = best_model_name

    # use best hyperparameters in training options
    if isinstance(trial, dict) :
        opt_best_model.beta1 = trial['params_beta1']
        opt_best_model.lr = trial['params_lr']
        opt_best_model.batch_size = trial['params_batch_size']
        opt_best_model.no_dropout = trial['params_no_dropout']
        opt_best_model.lr_policy = trial['params_lr_policy']
    else :
        opt_best_model.beta1 = trial.params['beta1']
        opt_best_model.lr = trial.params['lr']
        opt_best_model.batch_size = trial.params['batch_size']
        opt_best_model.no_dropout = trial.params['no_dropout']
        opt_best_model.lr_policy = trial.params['lr_policy']

    # run training with best hyperparameters
    TrainOptions().print_options(opt_best_model, parse_bool=False)
    MyTrain = Train(opt_best_model)

    # create directories for the outputs
    os.makedirs(results_dir + '/' + best_model_name, exist_ok=True)
    os.makedirs( './checkpoints/' + best_model_name, exist_ok=True)
    MyTrain.run_train()

    # define options for testing
    opt_test = Namespace(aspect_ratio=1.0, img_names=testing_subjects, batch_size=1, checkpoints_dir='./checkpoints',
                         crop_size=256,
                         dataroot=opt_best_model.dataroot, dataset_mode=opt_best_model.dataset_mode,
                         direction=opt_best_model.direction,
                         display_winsize=256, epoch='latest', eval=False, gpu_ids=[0],
                         init_gain=opt_best_model.init_gain, init_type='normal',
                         input_nc=3, isTrain=False, load_iter=0, load_size=256, max_dataset_size=float('inf'),
                         model=opt_best_model.model,
                         n_layers_D=3, name=opt_best_model.name, ndf=64, netD='basic', netG='unet_256', ngf=64,
                         no_dropout=opt_best_model.no_dropout,
                         no_flip=False, norm=opt_best_model.norm, ntest=float('inf'), num_test=50, num_threads=20,
                         output_nc=3, phase='cross_val',
                         preprocess='resize_and_crop', results_dir=results_dir, serial_batches=False, suffix='',
                         verbose=False)

    # run test
    MyTest = Test(opt_test)
    MyTest.run_test()

if __name__ == "__main__":
    # read csv folder that has n_rows = n_data and n_columns = n_fold, and for each fold specifies if the data point is for training/testing/validation
    data_split = pd.read_csv('./cv_folds.csv')

    for fold in [0,1] :
        global train_in
        global validation_in

        # define sets of data: a testing set for each fold and an outter training fold (train_out), made of inner training set (train_in) and some validation patients
        testing_subjects = data_split[data_split['fold_'+str(fold)]=='test']['patient'].tolist()
        validation_in = data_split[data_split['fold_' + str(fold)] == 'val']['patient'].tolist()
        train_in = data_split[data_split['fold_' + str(fold)] == 'train']['patient'].tolist()
        train_out = data_split[data_split['fold_' + str(fold)] != 'test']['patient'].tolist()

        # define training options
        opt_train.img_names = train_in
        # run optimization in this fold
        best_model = optuna_optimization()

        # retrieve best model
        best_model = read_optuna_results(fold).to_dict()
        best_model = {key:value[[*value][0]] for (key, value) in best_model.items()}

        # retrain the best model on whole train_out data and save results
        train_test_best_model(best_model, model_name + '_best_' + str(fold), train_out)

