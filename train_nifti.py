"""General-purpose training script for image-to-image translation.

This script works for various models (with option '--model': e.g., pix2pix, cyclegan, colorization) and
different datasets (with option '--dataset_mode': e.g., aligned, unaligned, single, colorization).
You need to specify the dataset ('--dataroot'), experiment name ('--name'), and model ('--model').

It first creates model, dataset, and visualizer given the option.
It then does standard network training. During the training, it also visualize/save the images, print/save the loss plot, and save models.
The script supports continue/resume training. Use '--continue_train' to resume your previous training.

Example:
    Train a CycleGAN model:
        python train.py --dataroot ./datasets/maps --name maps_cyclegan --model cycle_gan
    Train a pix2pix model:
        python train.py --dataroot ./datasets/facades --name facades_pix2pix --model pix2pix --direction BtoA

See options/base_options.py and options/train_options.py for more training options.
See training and test tips at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md
See frequently asked questions at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/qa.md
"""

import random
import time
from options.train_options import TrainOptions
from data import create_dataset, undo_normalization
from models import create_model
from PIL import Image
from util.visualizer import Visualizer
import torch
import numpy as np
from argparse import Namespace
import glob
import os
import pytorch_ssim as pytssim

class Train() :

    def __init__(self, opt):
        # get training options
        self.opt = opt
        self.opt_val = opt

        # iteration over validation patients
        if self.opt.enable_validation:
            self.opt_val.phase = 'val'
            self.opt_val.batch_size = 1
            self.validation_set = create_dataset(self.opt_val)

        # create a dataset given self.opt.dataset_mode and other options
        self.dataset = create_dataset(self.opt)

        # get the number of images in the dataset.
        dataset_size = len(self.dataset)
        print('The number of training images = %d' % dataset_size)

        # create a model given self.opt.model and other options
        self.model = create_model(self.opt)

        # regular setup: load and print networks; create schedulers
        self.model.setup(self.opt)

        # create a visualizer that display/save images and plots
        self.visualizer = Visualizer(self.opt)
        
        # the total number of training iterations
        self.n_channels = self.opt.input_nc
    
    def load_current_slice(self, direction, data, slice, i):
        '''
        function to load a given slice among a stack of x central slices in each orientation

        Args :
            direction : sag, cor or tra
            data : dataset generated
            slice ; the slice number to use
        '''
        if direction == 'sag':
            sub_data = {'A': data["A"][:, :, 88 + slice:88 + slice + self.n_channels, :, :].squeeze(1), \
                        'B': data["B"][:, :, 88 + slice:88 + slice + self.n_channels, :, :].squeeze(1), \
                        'A_paths': [data['A_paths'][0] + "_" + str(88 + slice)], \
                        'B_paths': [data['B_paths'][0] + "_" + str(88 + slice)]}
            if i == 0:
                img = torch.reshape(
                    torch.squeeze(sub_data["A"][:, 1, :, :]),
                    [sub_data["A"].size()[0], 1, sub_data["A"].size()[2],
                     sub_data["A"].size()[3]])[0, 0, :, :].squeeze(1).numpy()
                # uncomment this if you want to save snippets of the data loaded

                # I8 = (((img - img.min()) / (img.max() - img.min())) * 255.9).astype(np.uint8)
                # gr_im = Image.fromarray(I8).convert('L').save(sub_data['A_paths'][0].replace('./datasets/ROMENS/whole_head_Bn4_Areg/A/train/','./test/')+'_sag.png')

        if direction == 'tra':
            sub_data = {'A': torch.transpose(
                data["A"][:, :, :, 110 - 40 + slice:110 - 40 + slice + self.n_channels, :].squeeze(1), 2, 1),
                'B': torch.transpose(
                    data["B"][:, :, :, 110 - 40 + slice:110 - 40 + slice + self.n_channels, :].squeeze(
                        1), 2, 1),
                'A_paths': [data['A_paths'][0] + "_" + str(110 - 40 + slice)],
                'B_paths': [data['B_paths'][0] + "_" + str(110 - 40 + slice)]}
            if i == 0:
                img = torch.reshape(
                    torch.squeeze(sub_data["A"][:, 1, :, :]),
                    [sub_data["A"].size()[0], 1, sub_data["A"].size()[2],
                     sub_data["A"].size()[3]])[0, 0, :, :].squeeze(1).numpy()
                # I8 = (((img - img.min()) / (img.max() - img.min())) * 255.9).astype(np.uint8)
                # gr_im = Image.fromarray(I8).convert('L').save(sub_data['A_paths'][0].replace('./datasets/ROMENS/whole_head_Bn4_Areg/A/train/','./test/')+'_tra.png')

        if direction == 'cor':
            sub_data = {'A': torch.transpose(
                data["A"][:, :, :, :, 154 - 40 + slice:154 - 40 + slice + self.n_channels].squeeze(1), 3, 1),
                'B': torch.transpose(
                    data["B"][:, :, :, :, 154 - 40 + slice:154 - 40 + slice + self.n_channels].squeeze(
                        1), 3, 1),
                'A_paths': [data['A_paths'][0] + "_" + str(154 - 40 + slice)],
                'B_paths': [data['B_paths'][0] + "_" + str(154 - 40 + slice)]}
            if i == 0:
                img = torch.reshape(
                    torch.squeeze(sub_data["A"][:, 1, :, :]),
                    [sub_data["A"].size()[0], 1, sub_data["A"].size()[2],
                     sub_data["A"].size()[3]])[0, 0, :, :].squeeze(1).numpy()
                # I8 = (((img - img.min()) / (img.max() - img.min())) * 255.9).astype(np.uint8)
                # gr_im = Image.fromarray(I8).convert('L').save(sub_data['A_paths'][0].replace('./datasets/ROMENS/whole_head_Bn4_Areg/A/train/','./test/') + '_cor.png')
        
        return sub_data
    
    def do_validation(self, direction):
        ssim_val = []
        # for each data in validatoin
        for j, val_data in enumerate(self.validation_set):
            ssim_slices = []
            # iteration over slices
            for slice_val in range(3):
                # define data for validation, depending on orientation
                if direction == 'sag':
                    sub_data_validation = {
                        'A': val_data["A"][:, :, 88 + slice_val:88 + slice_val + self.n_channels, :,
                             :].squeeze(1), \
                        'B': val_data["B"][:, :, 88 + slice_val:88 + slice_val + self.n_channels, :,
                             :].squeeze(1), \
                        'A_paths': [val_data['A_paths'][0] + "_" + str(88 + slice_val)], \
                        'B_paths': [val_data['B_paths'][0] + "_" + str(88 + slice_val)]}
                if direction == 'tra':
                    sub_data_validation = {'A': torch.transpose(val_data["A"][:, :, :,
                                                                110 - 40 + slice_val:110 - 40 + slice_val + self.n_channels,
                                                                :].squeeze(1), 2, 1),
                                           'B': torch.transpose(val_data["B"][:, :, :,
                                                                110 - 40 + slice_val:110 - 40 + slice_val + self.n_channels,
                                                                :].squeeze(1), 2, 1),
                                           'A_paths': [val_data['A_paths'][0] + "_" + str(
                                               110 - 40 + slice_val)],
                                           'B_paths': [val_data['B_paths'][0] + "_" + str(
                                               110 - 40 + slice_val)]}
                if direction == 'cor':
                    sub_data_validation = {'A': torch.transpose(val_data["A"][:, :, :, :,
                                                                154 - 40 + slice_val:154 - 40 + slice_val + self.n_channels].squeeze(
                        1), 3, 1),
                        'B': torch.transpose(val_data["B"][:, :, :, :,
                                             154 - 40 + slice_val:154 - 40 + slice_val + self.n_channels].squeeze(
                            1), 3, 1),
                        'A_paths': [val_data['A_paths'][0] + "_" + str(
                            154 - 40 + slice_val)],
                        'B_paths': [val_data['B_paths'][0] + "_" + str(
                            154 - 40 + slice_val)]}

                # unpack data from data loader
                self.model.set_input(sub_data_validation)
                # run inference
                self.model.test()
                # get image results
                visuals_validation = self.model.get_current_visuals()
                # compute ssim between two slices
                ssim_slices.append(pytssim.ssim(visuals_validation['fake_B'].cuda(),
                                                visuals_validation['real_B'].cuda()))
            ssim_val.append(torch.mean(torch.stack(ssim_slices)))

        # similarity losses
        self.model.get_training_loss(ssim_val)
    
    def print_visualizer(self, total_iters, epoch):
        # display images on visdom and save images to a HTML file
        if total_iters % self.opt.display_freq == 0:
            save_result = total_iters % self.opt.update_html_freq == 0
            self.model.compute_visuals()
            if self.n_channels > 1:
                current_visuals = self.model.get_current_visuals()
                current_visuals["real_A"] = torch.reshape(
                    torch.squeeze(current_visuals["real_A"][:, int(self.n_channels / 2), :, :]),
                    [current_visuals["real_A"].size()[0], 1, current_visuals["real_A"].size()[2],
                     current_visuals["real_A"].size()[3]])
                current_visuals["real_B"] = torch.reshape(
                    torch.squeeze(current_visuals["real_B"][:, int(self.n_channels / 2), :, :]),
                    [current_visuals["real_A"].size()[0], 1, current_visuals["real_B"].size()[2],
                     current_visuals["real_B"].size()[3]])
                current_visuals["fake_B"] = torch.reshape(
                    torch.squeeze(current_visuals["fake_B"][:, int(self.n_channels / 2), :, :]),
                    [current_visuals["real_A"].size()[0], 1, current_visuals["fake_B"].size()[2],
                     current_visuals["fake_B"].size()[3]])
                self.visualizer.display_current_results(current_visuals, epoch, save_result)
            else:
                self.visualizer.display_current_results(self.model.get_current_visuals(), epoch, save_result)
                
    def print_losses(self, total_iters, iter_start_time, epoch, epoch_iter, t_data):
        # print training losses and save logging information to the disk
        if total_iters % self.opt.print_freq == 0:
            # get training + valdation losses
            losses = self.model.get_current_losses()
            # time elapsed for given epoch
            t_comp = (time.time() - iter_start_time) / self.opt.batch_size
            # print current losses
            self.visualizer.print_current_losses(epoch, epoch_iter, losses, t_comp, t_data)
            # plot current losses
            if self.opt.display_id > 0:
                self.visualizer.plot_current_losses(epoch,
                                                    float(epoch_iter) / (len(self.dataset) * 256 - self.n_channels),
                                                    losses)
                
    def run_train(self) :
        total_iters = 0
        # outer loop for different epochs; we save the model by <epoch_count>, <epoch_count>+<save_latest_freq>
        for epoch in range(self.opt.epoch_count, self.opt.niter + self.opt.niter_decay + 1):
            epoch_start_time = time.time()  # timer for entire epoch
            iter_data_time = time.time()  # timer for data loading per iteration
            epoch_iter = 0  # the number of training iterations in current epoch, reset to 0 every epoch
            
            # iteration within one about over training images
            for i, data in enumerate(self.dataset):
                # iterate over slices in 3D volume (added from original implementation)
                # [batch,channel,z,y,x]
                my_range = list(range(80 - self.n_channels))
                # shuffle order of slices
                random.shuffle(my_range)
                # iterate over slices whitin the considered range
                for slice in my_range:
                    # iterate over directions
                    for direction in ['sag', 'cor', 'tra']:
                        # load the current slice
                        sub_data = self.load_current_slice(direction, data, slice, i)
                        
                        # timer for computation per iteration
                        iter_start_time = time.time()
                        if total_iters % self.opt.print_freq == 0:
                            t_data = iter_start_time - iter_data_time

                        # visualizer
                        self.visualizer.reset()
                        total_iters += self.opt.batch_size
                        epoch_iter += self.opt.batch_size

                        # unpack data from dataset and apply preprocessing
                        self.model.set_input(sub_data)

                        # if the adversarial loss most be used
                        # calculate loss functions, get gradients, update network weights
                        if epoch < self.opt.switch_gan_loss:
                            self.model.optimize_parameters(gan_bool=False)
                        else:
                            self.model.optimize_parameters(gan_bool=True)

                        if any([np.isnan(item) for item in self.model.get_current_losses().values()]) :
                            print('NAN found in losses - aborting')
                            return

                        # if validation is enabled
                        if self.opt.enable_validation and total_iters % self.opt.print_freq == 0:
                            self.do_validation(direction)

                        self.print_visualizer(total_iters, epoch)

                        self.print_losses(total_iters, iter_start_time, epoch, epoch_iter, t_data)
                        
                        # cache our latest model every <save_latest_freq> iterations
                        if total_iters % self.opt.save_latest_freq == 0:
                            print('saving the latest model (epoch %d, total_iters %d)' % (epoch, total_iters))
                            save_suffix = 'iter_%d' % total_iters if self.opt.save_by_iter else 'latest'
                            self.model.save_networks(save_suffix)

                        iter_data_time = time.time()

            # cache our model every <save_epoch_freq> epochs
            if epoch % self.opt.save_epoch_freq == 0:
                print('saving the model at the end of epoch %d, iters %d' % (epoch, total_iters))
                self.model.save_networks('latest')
                self.model.save_networks(epoch)

            print('End of epoch %d / %d \t Time Taken: %d sec' % (
            epoch, self.opt.niter + self.opt.niter_decay, time.time() - epoch_start_time))

            # update learning rates at the end of every epoch.
            self.model.update_learning_rate()